#!/bin/bash

for f in *; do
    if [[ -d ${f} ]]; then
      echo "Updating submodules in ${f}"
      cd ${f}
      git submodule update --init --recursive
      cd ..
    fi
done
