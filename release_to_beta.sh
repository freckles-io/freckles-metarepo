#!/usr/bin/env bash

devpi use https://pkgs.frkl.io/
devpi login frkl --password="${DEVPI_PASSWORD}"
devpi use /frkl/beta

for f in *; do
    if [[ -d ${f} ]]; then
      echo "Uploading to /frkl/beta: ${f}"
      rm -rf dist build
      cd ${f}
      devpi upload
      cd ..
    fi
done
